//
//  LoginViewController.swift
//  PasswordHolder
//
//  Created by Samuel Mensak on 03/06/2019.
//  Copyright © 2019 Samuel Mensak. All rights reserved.
//

import Foundation
import UIKit
import LocalAuthentication
import RealmSwift

protocol LoginVCDelegate: AnyObject {
    func authentificated()
    func failed()
}

class LoginViewController: UIViewController {
    var counter: Int = 0
    var pin: String = ""
    var enteredPin: String = ""
    var biometricsAvailable: Bool = false
    var biometricsEnabled: Bool = true
    var loginToApp: Bool?
    weak var delegate: LoginVCDelegate?
    let localAuthenticationContext = LAContext()
    @IBOutlet weak var PIN1: UIImageView!
    @IBOutlet weak var PIN2: UIImageView!
    @IBOutlet weak var PIN3: UIImageView!
    @IBOutlet weak var PIN4: UIImageView!
    @IBOutlet var buttons: [UIButton]!
    //
    override func viewDidAppear(_ animated: Bool) {
        // if biometry is enabled, skip PIN and try to authentificate user via biometry
        if self.biometricsEnabled {
            let reason = "Log in to your account"
            localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics,
                                                      localizedReason: reason ) { success, error in
                if success && self.biometricsAvailable {
                    if self.loginToApp == true {
                        DispatchQueue.main.async {
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let dvc = storyboard.instantiateViewController(withIdentifier: "MainNavigationController")
                                as? UINavigationController
                            self.present(dvc!, animated: true)
                        }
                    } else {
                        DispatchQueue.main.async {
                            self.delegate?.authentificated()
                            self.presentingViewController?.dismiss(animated: true, completion: nil)
                        }
                    }
                } else {
                    print(error?.localizedDescription ?? "Failed to authenticate")
                }
            }
        }
    }
    //
    override func viewWillAppear(_ animated: Bool) {
        // set up buttons appearance
        for button in self.buttons {
            button.layer.cornerRadius = button.frame.height / 2
            button.layer.borderWidth = 1.5
            button.layer.borderColor = UIColor(red: 0.00, green: 0.48, blue: 1.00, alpha: 1.0).cgColor
        }
        PIN1.layer.cornerRadius = PIN1.frame.height / 2
        PIN2.layer.cornerRadius = PIN2.frame.height / 2
        PIN3.layer.cornerRadius = PIN3.frame.height / 2
        PIN4.layer.cornerRadius = PIN4.frame.height / 2
        // check for biometrics
        var error: NSError?
        if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
            biometricsAvailable = true
        }
        // update settings / preferences
        let realm = try? Realm()
        let preferences = realm!.objects(UserPref.self).first
        pin = String(preferences!.PIN)
        biometricsEnabled = Bool(preferences!.biometryEnabled)
    }
    //
    @IBAction func backButton(_ sender: UIButton) {
        if counter > 0 {
            // delete last digit
            redrawPinBar()
            counter -= 1
            updatePinBar()
            enteredPin = String(enteredPin.dropLast())
        }
        // when not loging ito app and PIN not entered, back button will close login VC
        if loginToApp == false && counter == 0 {
            DispatchQueue.main.async {
                self.delegate?.failed()
                self.presentingViewController?.dismiss(animated: true, completion: nil)
            }
        }
    }
    //
    @IBAction func keyPadClicked(_ sender: UIButton) {
        // add entered digit to PIN
        let tmp = sender.titleLabel!.text
        enteredPin.append(String(tmp!))
        // if 4 digits entered, verify PIN
        if counter == 3 {
            verifyPin()
        }
        // else update pinbar
        counter += 1
        updatePinBar()
        if counter >= 4 {
            counter = 0
            updatePinBar()
        }
    }
    //
    func verifyPin() {
        // if PIN does match
        if enteredPin == pin {
            if loginToApp == true {
                // if logging into app, show main VC
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let dvc = storyboard.instantiateViewController(withIdentifier: "MainNavigationController") as?
                UINavigationController
                present(dvc!, animated: true)
            } else {
                // let delegate know about success and dismiss
                DispatchQueue.main.async {
                    self.delegate?.authentificated()
                    self.presentingViewController?.dismiss(animated: true, completion: nil)
                }
            }
        } else {
            // if failed to auth., display alert
            let alert = UIAlertController(title: "Incorrect PIN",
                                          message: "We were unable to autentificate you due to incorrect PIN",
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Try again", style: .cancel, handler: nil))
            self.present(alert, animated: true)
            enteredPin = ""
        }
    }
    //
    func updatePinBar() {
        switch counter {
        case 4:
            PIN4.backgroundColor = UIColor.black
            fallthrough
        case 3:
            PIN3.backgroundColor = UIColor.black
            fallthrough
        case 2:
            PIN2.backgroundColor = UIColor.black
            fallthrough
        case 1:
            PIN1.backgroundColor = UIColor.black
        case 0:
            redrawPinBar()
        default:
            print("error")
        }
    }
    //
    func redrawPinBar() {
        PIN1.backgroundColor = UIColor.gray
        PIN2.backgroundColor = UIColor.gray
        PIN3.backgroundColor = UIColor.gray
        PIN4.backgroundColor = UIColor.gray
    }
}
