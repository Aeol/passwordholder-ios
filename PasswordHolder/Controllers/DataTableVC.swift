//
//  DataTableVC.swift
//  PasswordHolder
//
//  Created by Samuel Mensak on 11/06/2019.
//  Copyright © 2019 Samuel Mensak. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import LocalAuthentication

class DataTableVC: UITableViewController, NewAccountVCDelegate, UISearchBarDelegate, LoginVCDelegate {
    let realm = try! Realm()
    var model: Results<Account>!
    @IBOutlet weak var searchBar: UISearchBar!
    var lastSelectedIndexPath: IndexPath!
    //
    func newAccountDefined() {
        self.tableView.reloadData()
    }
    //
    func accountEdited() {
        self.tableView.reloadData()
    }
    //
    func authentificated() {
        // if authentificated, show detail
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "DetailEntry", sender: self)
        }
    }
    //
    func failed() {
        // if not authentificated
        //print("failed")
    }
    //
    override func viewDidLoad() {
        searchBar.delegate = self
        model = realm.objects(Account.self)
        tableView.tableFooterView = UIView()
    }
    //
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // search logic using Realm filter
        let searchSettings = realm.objects(SearchSettings.self).first
        var predicates: [NSPredicate] = []
        // creating predicate for filter based on search settings
        if searchSettings!.server == true {
            predicates.append(NSPredicate(format: "server CONTAINS %@", searchText))
        }
        if searchSettings!.username == true {
            predicates.append(NSPredicate(format: "username CONTAINS %@", searchText))
        }
        if searchSettings!.email == true {
            predicates.append(NSPredicate(format: "email CONTAINS %@", searchText))
        }
        if searchSettings!.note == true {
            predicates.append(NSPredicate(format: "note CONTAINS %@", searchText))
        }
        let predicate = NSCompoundPredicate(orPredicateWithSubpredicates: predicates)
        // updating model to reflect search request
        model = searchText.isEmpty ? realm.objects(Account.self) : realm.objects(Account.self).filter(predicate)
        self.tableView.reloadData()
    }
    //
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    //
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }
    //
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AccountCell") as? AccountTableViewCell
        let account = model[indexPath.row]
        cell!.config(account: account)
        return cell!
    }
    //
    public override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    //
    public override func tableView(_ tableView: UITableView,
                                   commit editingStyle: UITableViewCell.EditingStyle,
                                   forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            try! realm.write {
                realm.delete(model[indexPath.row])
            }
            self.tableView.reloadData()
        }
    }
    //
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // remember last item selected by user
        lastSelectedIndexPath = tableView.indexPathForSelectedRow
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "login", sender: self)
        }
    }
    //
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return true
    }
    //
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "NewEntry" {
            let dvc = segue.destination as? AccountDetailVC
            dvc!.delegate = self
            dvc!.creatingNewAcc = true
        }
        if segue.identifier == "DetailEntry" {
            let dvc = segue.destination as? AccountDetailVC
            dvc!.delegate = self
            dvc!.creatingNewAcc = false
            dvc!.existingAccount = model[lastSelectedIndexPath!.row]
        }
        if segue.identifier == "login" {
            let dvc = segue.destination as? LoginViewController
            dvc!.delegate = self
            dvc?.loginToApp = false
        }
    }
}
