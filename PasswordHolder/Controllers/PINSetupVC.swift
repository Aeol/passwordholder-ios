//
//  PINSetupVC.swift
//  PasswordHolder
//
//  Created by Samuel Mensak on 10/06/2019.
//  Copyright © 2019 Samuel Mensak. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class PINSetupVC: UIViewController {
    var enteredPin = ""
    var pin1 = ""
    var firstEntry: Bool = true
    var counter: Int = 0
    var creatingNewPIN: Bool?
    var setup: Bool = true
    //
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var PINDot1: UIImageView!
    @IBOutlet weak var PINDot2: UIImageView!
    @IBOutlet weak var PINDot3: UIImageView!
    @IBOutlet weak var PINDot4: UIImageView!
    @IBOutlet var buttons: [UIButton]!

    override func viewWillAppear(_ animated: Bool) {
        if creatingNewPIN == true {
            descLabel.text = "Enter your existing PIN"
        }
        for button in self.buttons {
            button.layer.cornerRadius = button.frame.height / 2
            button.layer.borderWidth = 1.5
            button.layer.borderColor = UIColor(red: 0.00, green: 0.48, blue: 1.00, alpha: 1.0).cgColor
        }
        let radius = PINDot1.frame.height / 2
        PINDot1.layer.cornerRadius = radius
        PINDot2.layer.cornerRadius = radius
        PINDot3.layer.cornerRadius = radius
        PINDot4.layer.cornerRadius = radius
        firstEntry = true
    }
    //
    @IBAction func buttonClicked(_ sender: UIButton) {
        // add pressed number to entered PIN
        let tmp = sender.titleLabel!.text
        enteredPin.append(String(tmp!))
        // if 4 numbers entered, verify PIN
        if counter == 3 {
            verifyPin()
        }
        // update pinbar, counter
        counter += 1
        updatePinBar()
        if counter >= 4 {
            counter = 0
            updatePinBar()
        }
    }
    //
    @IBAction func undoButton(_ sender: UIButton) {
        // remove last digit from entered pin
        if counter > 0 {
            redrawPinBar()
            counter -= 1
            updatePinBar()
            enteredPin = String(enteredPin.dropLast())
        }
    }
    //
    func verifyPin() {
        // if new PIN is being set up
        if creatingNewPIN == true {
            let realm = try! Realm()
            let preferences = realm.objects(UserPref.self).first
            if enteredPin == preferences?.PIN {
                creatingNewPIN = false
                setup = false
                descLabel.text = "Enter new PIN"
            } else {
                let alert = UIAlertController(title: "PIN does not match",
                            message: "PIN entered doesnt match one currently used, try again!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel))
                self.present(alert, animated: true)
            }
            enteredPin = ""
            updatePinBar()
        // if first PIN is beeing created
        } else if firstEntry {
            pin1 = enteredPin
            enteredPin = ""
            descLabel.text = "Enter your PIN again"
            firstEntry = false
        // if 2nd PIN entered matches first entry
        } else if enteredPin == pin1 {
            // new PIN was set up
            if setup == false {
                let alert = UIAlertController(title: "PIN changed",
                            message: "click OK to continue", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { _ in
                self.dismiss(animated: true, completion: nil)}))
                self.present(alert, animated: true)
            }
            // if PIN was set up for a first time
            enteredPin = ""
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let dvc = storyboard.instantiateViewController(withIdentifier: "SetupDoneVC") as? SetupDoneVC
            present(dvc!, animated: true)
            // update settings
            let realm = try! Realm()
            let preferences = realm.objects(UserPref.self).first
            try! realm.write { preferences!.PIN = pin1 }
        // if 2nd PIN doesnt match first PIN entered
        } else {
            let alert = UIAlertController(title: "PIN does not match",
                        message: "PIN entered doesnt match first entry, try again!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel))
            self.present(alert, animated: true)
            enteredPin = ""
            updatePinBar()
        }
    }
    //
    func updatePinBar() {
        switch counter {
        case 4:
            PINDot4.backgroundColor = UIColor.black
            fallthrough
        case 3:
            PINDot3.backgroundColor = UIColor.black
            fallthrough
        case 2:
            PINDot2.backgroundColor = UIColor.black
            fallthrough
        case 1:
            PINDot1.backgroundColor = UIColor.black
        case 0:
            redrawPinBar()
        default:
            print("error")
        }
    }
    //
    func redrawPinBar() {
        PINDot1.backgroundColor = UIColor.gray
        PINDot2.backgroundColor = UIColor.gray
        PINDot3.backgroundColor = UIColor.gray
        PINDot4.backgroundColor = UIColor.gray
    }
}
