//
//  SetupViewController.swift
//  PasswordHolder
//
//  Created by Samuel Mensak on 09/06/2019.
//  Copyright © 2019 Samuel Mensak. All rights reserved.
//

import Foundation
import UIKit

class SetupViewController: UIViewController {
    @IBOutlet weak var button: UIButton!
    //
    override func viewWillAppear(_ animated: Bool) {
        button.layer.cornerRadius = 15
    }
}
