//
//  PrePINSetupVC.swift
//  PasswordHolder
//
//  Created by Samuel Mensak on 09/06/2019.
//  Copyright © 2019 Samuel Mensak. All rights reserved.
//

import Foundation
import UIKit

class PrePINSetupVC: UIViewController {
    var biometryEnabled: Bool?
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var infoText: UITextView!
    //
    override func viewWillAppear(_ animated: Bool) {
        button.layer.cornerRadius = 15
        infoText.text = "Biometric login was "
        infoText.text.append(biometryEnabled! ? "enabled" : "disabled")
        infoText.text.append("!\n\nLet's set up a PIN")
    }
}
