//
//  SetupDoneVC.swift
//  PasswordHolder
//
//  Created by Samuel Mensak on 11/06/2019.
//  Copyright © 2019 Samuel Mensak. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class SetupDoneVC: UIViewController {
    @IBOutlet weak var button: UIButton!
    //
    override func viewWillAppear(_ animated: Bool) {
        button.layer.cornerRadius = 15
        let realm = try! Realm()
        let preferences = realm.objects(UserPref.self).first
        // setup successfull, note that in preferences
        try! realm.write {
            preferences?.setupCompleted = true
        }
        // default setting
        let settings = SearchSettings()
        try! realm.write {
            realm.add(settings)
        }
    }
}
