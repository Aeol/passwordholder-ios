//
//  SettingsVC.swift
//  PasswordHolder
//
//  Created by Samuel Mensak on 11/06/2019.
//  Copyright © 2019 Samuel Mensak. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class SettingsVC: UITableViewController {
    @IBOutlet weak var biometricsSwitch: UISwitch!
    @IBOutlet weak var serverSwitch: UISwitch!
    @IBOutlet weak var usernameSwitch: UISwitch!
    @IBOutlet weak var emailSwitch: UISwitch!
    @IBOutlet weak var noteSwitch: UISwitch!
    let realm = try! Realm()
    //
    @IBAction func biometricsSwiched(_ sender: UISwitch) {
        // get user settings and search settings objects from realm
        let settings = realm.objects(UserPref.self).first
        let searchSettings = realm.objects(SearchSettings.self).first
        // update settings
        try! realm.write {
            settings?.biometryEnabled = biometricsSwitch.isOn
            searchSettings?.server = serverSwitch.isOn
            searchSettings?.username = usernameSwitch.isOn
            searchSettings?.email = emailSwitch.isOn
            searchSettings?.note = noteSwitch.isOn
        }
    }
    //
    override func viewWillAppear(_ animated: Bool) {
        // get settings
        let settings = realm.objects(UserPref.self).first
        let searchSettings = realm.objects(SearchSettings.self).first
        // set switches
        biometricsSwitch.setOn(settings!.biometryEnabled, animated: true)
        serverSwitch.setOn(searchSettings!.server, animated: true)
        usernameSwitch.setOn(searchSettings!.username, animated: true)
        emailSwitch.setOn(searchSettings!.email, animated: true)
        noteSwitch.setOn(searchSettings!.note, animated: true)
    }
    //
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "changePIN" {
            let dvc = segue.destination as? PINSetupVC
            dvc?.creatingNewPIN = true
        }
    }
}
