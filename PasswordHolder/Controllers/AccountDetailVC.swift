//
//  NewAccountVC.swift
//  PasswordHolder
//
//  Created by Samuel Mensak on 12/06/2019.
//  Copyright © 2019 Samuel Mensak. All rights reserved.
//

import UIKit
import RealmSwift

protocol NewAccountVCDelegate: AnyObject {
    func newAccountDefined()
    func accountEdited()
}

class AccountDetailVC: UITableViewController {
    let realm = try! Realm()
    weak var delegate: NewAccountVCDelegate?

    @IBOutlet weak var navbar: UINavigationItem!
    @IBOutlet weak var server: UITextField!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var note: UITextView!
    var existingAccount: Account?
    var creatingNewAcc: Bool?
    //
    override func viewDidLoad() {
        // set title (creating new acc or editing existing one)
        navbar.title = creatingNewAcc! ? "New Account" : existingAccount?.server
    }
    //
    override func viewWillAppear(_ animated: Bool) {
        // if showing / editing existing account, load date
        if creatingNewAcc == false {
            server.text = existingAccount?.server
            username.text = existingAccount?.username
            email.text = existingAccount?.email
            password.text = existingAccount?.password
            note.text = existingAccount?.note
        }
    }
    //
    @IBAction func saveNote(_ sender: UIBarButtonItem) {
        let account = Account()
        // chceck if required fields are filled in
        if server.text == "" {
            let avc = UIAlertController(title: "Empty field", message:
                "Server name should not be empty!", preferredStyle: .alert)
            let action = UIAlertAction(title: "ok", style: .default, handler: nil)
            avc.addAction(action)
            present(avc, animated: true, completion: nil)
            return
        }
        if username.text == "" && email.text == "" {
            let avc = UIAlertController(title: "Empty field",
                                       message: "Username or e-mail address must be entered!", preferredStyle: .alert)
            let action = UIAlertAction(title: "ok", style: .default, handler: nil)
            avc.addAction(action)
            present(avc, animated: true, completion: nil)
            return
        }
        if password.text == "" {
            let avc = UIAlertController(title: "Empty field", message: "Password should not be empty!",
                                        preferredStyle: .alert)
            let action = UIAlertAction(title: "ok", style: .default, handler: nil)
            avc.addAction(action)
            present(avc, animated: true, completion: nil)
            return
        }
        // if updating existing account
        if creatingNewAcc == false {
            let realm = try? Realm()
            try? realm!.write {
                existingAccount?.server = server.text!
                existingAccount?.username = username.text!
                existingAccount?.email = email.text!
                existingAccount?.password = password.text!
                existingAccount?.note = note.text!
                DispatchQueue.main.async { self.delegate?.accountEdited()}}
        // if creating new account
        } else {
            account.server = server.text!
            account.username = username.text!
            account.email = email.text!
            account.password = password.text!
            account.note = note.text!
            try! realm.write { realm.add(account) }
            DispatchQueue.main.async { self.delegate?.newAccountDefined() }
        }
        // the end, dismiss
        navigationController?.popViewController(animated: true)
    }
}
