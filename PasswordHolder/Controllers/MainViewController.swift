//
//  MainViewController.swift
//  PasswordHolder
//
//  Created by Samuel Mensak on 09/06/2019.
//  Copyright © 2019 Samuel Mensak. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class MainViewController: UIViewController {
    let realm = try! Realm()
    var firstStartup: Bool = true
    //
    override func viewWillAppear(_ animated: Bool) {
        // load preferences, if not yet set, create them
        let preferences = realm.objects(UserPref.self).first
        if preferences == nil {
            let pref = UserPref()
            pref.setupCompleted = false
            try! realm.write {
                realm.add(pref)
            }
        }
        if preferences?.setupCompleted == true {
            firstStartup = false
        }
        if preferences?.setupCompleted == false {
            firstStartup = true
        }
    }
    //
    override func viewDidAppear(_ animated: Bool) {
        if firstStartup {
            // show setup VC
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let dvc = storyboard.instantiateViewController(withIdentifier: "SetupViewController") as?
            SetupViewController
            present(dvc!, animated: true)
        } else {
            // show login VC
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let dvc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as?
            LoginViewController
            dvc!.loginToApp = true
            present(dvc!, animated: true)
        }
    }
}
