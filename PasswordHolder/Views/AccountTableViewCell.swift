//
//  AccountTableViewCell.swift
//  PasswordHolder
//
//  Created by Samuel Mensak on 11/06/2019.
//  Copyright © 2019 Samuel Mensak. All rights reserved.
//

import Foundation
import UIKit

class AccountTableViewCell: UITableViewCell {
    @IBOutlet weak var serverName: UILabel!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    //
    func config(account: Account) {
        serverName?.text = account.server
        username?.text = account.username
        email?.text = account.email
        password?.text = account.password
        titleLabel?.text = String(account.server.prefix(1).uppercased())
        // bring some color into view
        let red: CGFloat = CGFloat(drand48())
        let green: CGFloat = CGFloat(drand48())
        let blue: CGFloat = CGFloat(drand48())
        titleLabel.backgroundColor = UIColor(red: red, green: green, blue: blue, alpha: 1.0)
    }
}
