//
//  Account.swift
//  PasswordHolder
//
//  Created by Samuel Mensak on 11/06/2019.
//  Copyright © 2019 Samuel Mensak. All rights reserved.
//

import Foundation
import RealmSwift

public class Account: Object {
    @objc public dynamic var server = ""
    @objc public dynamic var username = ""
    @objc public dynamic var email = ""
    @objc public dynamic var password = ""
    @objc public dynamic var note = ""
}
