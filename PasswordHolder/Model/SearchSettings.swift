//
//  Settings.swift
//  PasswordHolder
//
//  Created by Samuel Mensak on 11/06/2019.
//  Copyright © 2019 Samuel Mensak. All rights reserved.
//

import Foundation
import RealmSwift

class SearchSettings: Object {
    @objc dynamic var server = true
    @objc dynamic var username = true
    @objc dynamic var email = true
    @objc dynamic var note = true
}
