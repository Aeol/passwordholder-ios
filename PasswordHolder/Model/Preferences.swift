//
//  Preferences.swift
//  PasswordHolder
//
//  Created by Samuel Mensak on 03/06/2019.
//  Copyright © 2019 Samuel Mensak. All rights reserved.
//

import Foundation
import RealmSwift

class UserPref: Object {
    @objc dynamic var PIN = ""
    @objc dynamic var biometryEnabled = true
    @objc dynamic var setupCompleted = false
}
