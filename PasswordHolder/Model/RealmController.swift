//
//  controller_realm.swift
//  PasswordHolder
//
//  Created by Samuel Mensak on 12/06/2019.
//  Copyright © 2019 Samuel Mensak. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

extension UIViewController {
    func getRealm() -> Realm? {
        guard let realm = try? Realm() else {
            alert(title: "Realm error", message: "Can't get realm")
            return nil
        }
        return realm
    }
    //
    func alert(title: String, message: String) {
        let act = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let done = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        act.addAction(done)
        present(act, animated: true, completion: nil)
    }
    //
    func performRealmAction<R> (action: () throws -> R?) -> R? {
        do { return try action()
        } catch let err {
            alert(title: "Realm action error", message: err.localizedDescription)
            return nil
        }
    }
}
